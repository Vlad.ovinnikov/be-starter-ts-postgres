# Authorization service

## Features

* Babel 7
* Environment Variables
* Express
* REST API
* PostgreSQL

## Requirements

## DB
### Generate model
npx sequelize-cli model:generate --name User --attributes firstName:string,lastName:string,email:string

### Migration
npx sequelize-cli db:migrate

https://github.com/rwieruch/node-express-postgresql-server
https://github.com/purpose50/todo-app/blob/postgres/models/index.js
https://github.com/grizzlypeaksoftware/buildingmicroservicesinnodejs
