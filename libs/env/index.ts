const authAPI = (): string => {
    return process.env.AUTH || 'http://localhost:8401';
};

const profileAPI = (): string => {
    return process.env.PROFILE || 'http://localhost:8402';
};

const prvGetaway = (): string => {
    return process.env.PRV_GETAWAY || 'http://localhost:8410';
};

export {
    authAPI, profileAPI, prvGetaway
};
