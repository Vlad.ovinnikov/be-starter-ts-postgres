import * as axios from 'axios';
import { AxiosResponse, AxiosRequestConfig } from 'axios';

import { prvGetaway } from '../../env';

const postProfileService = async (url: string, data) => {
    const requestConfig: AxiosRequestConfig = {
        headers: {
            'Content-Type': 'application/json'
        },
        url: `${ prvGetaway() }/internal/profiles/${ url }`,
        method: 'post',
        data
    };

    try {
        const response: AxiosResponse = await axios.default(requestConfig);
        return response.data;
    } catch (error) {
        return `Error connecting to profile service: ${ error }`;
    }
};

export default postProfileService;
