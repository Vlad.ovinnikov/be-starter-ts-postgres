
export enum UserRole {
    SuperAdmin = 0,
    Admin = 1,
    User = 2
}
