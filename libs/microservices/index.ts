import IConfig from './IConfig';
import { normalizeEmail } from './email';

export {
    IConfig, normalizeEmail
};
