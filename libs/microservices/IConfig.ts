export default interface IConfig {
    name: string;
    username: string;
    password: string;
    database: string;
    host: string;
    dialect: 'postgres';
    apiPort: number;
    logging: boolean;
}
