
export const normalizeEmail = (email: String): string | Error => {
    if (!email || !email.includes('@')) {
        throw new Error('Not email');
    }

    const parts = email.split('@');
    const name = parts[0].toLowerCase();
    const domain = parts[1].toLowerCase();

    return `${ name }@${ domain }`;
};
