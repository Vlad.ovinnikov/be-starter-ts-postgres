FROM node:12-alpine

WORKDIR /usr/app

RUN npm install -g yarn
RUN yarn -g add nodemon typescript@3.6.3 tslint

COPY ./libs ./libs
WORKDIR /usr/app/libs

RUN yarn
# RUN find ./libs/ yarn

WORKDIR /usr/app

COPY ./services/gateway ./services/gateway
WORKDIR /usr/app/services/gateway
RUN yarn
# RUN find ./services/gateway/ yarn

# EXPOSE 8400

CMD ["yarn", "start"]
