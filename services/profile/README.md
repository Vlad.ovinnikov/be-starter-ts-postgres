# x-project

## Features

* Babel 7
* Environment Variables
* Express
* REST API
* PostgreSQL

## Migration

Make migration:

``yarn migrate``

Undo migration:

``yarn migrate-undo``


https://github.com/rwieruch/node-express-postgresql-server
https://github.com/purpose50/todo-app/blob/postgres/models/index.js
https://github.com/grizzlypeaksoftware/buildingmicroservicesinnodejs
