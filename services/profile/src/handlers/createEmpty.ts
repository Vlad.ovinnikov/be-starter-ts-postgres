import { Request, Response, Router } from 'express';
import { CREATED, INTERNAL_SERVER_ERROR } from 'http-status-codes';

const router: Router = Router();

router.post(
  '/create-empty',
  async (req: Request, res: Response): Promise<Response> => {
    try {
        // const identities: IIdentityModel[] = await IdentityModel.findAll();
      console.log(req.body);

      return res.status(CREATED)
        .send(req.body);

    } catch (error) {
        return res.status(INTERNAL_SERVER_ERROR)
            .send(`Error creating empty profile: ${ error }`);
    }
});

export default router;
