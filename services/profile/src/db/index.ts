import { Sequelize } from 'sequelize';

import { IConfig } from '../../../../libs/microservices';

// import { IdentityStatic, IIdentityModel } from './identity';
// import { RefreshTokenStatic, IRefreshTokenModel } from './refreshTokens';
// import { VerificationCodeStatic, IVerificationCodeModel } from './verificationCodes';

const env: string = process.env.NODE_ENV || 'development';
const config: IConfig = require(`${ __dirname }/../../config/config.json`)[env];

const sequelize: Sequelize = new Sequelize(config.database, config.username, config.password, config);

// const IdentityModel: IdentityStatic = sequelize.import(`${ __dirname }/identity.ts`);
// const RefreshTokenModel: RefreshTokenStatic = sequelize.import(`${ __dirname }/refreshTokens.ts`);
// const VerificationCodeModel: VerificationCodeStatic = sequelize.import(`${ __dirname }/verificationCodes.ts`);

export {
    // IIdentityModel, IdentityModel,
    // IRefreshTokenModel, RefreshTokenModel,
    // IVerificationCodeModel, VerificationCodeModel,
    sequelize
};
