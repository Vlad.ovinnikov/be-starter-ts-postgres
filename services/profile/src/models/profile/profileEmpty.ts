export interface IProfileEmpty {
    readonly userId: number;
    readonly email: string;
    readonly username: string;
    readonly dateOfBirth: string;
    readonly firstName?: string;
    readonly lastName?: string;
    readonly addressId?: number;
    readonly paymentMethodId?: number;
    readonly avatarId?: number;
    // readonly createdAt: string;
    // readonly updatedAt?: string;
}

export class ProfileEmpty implements IProfileEmpty {

    userId: number = 0;
    email: string = '';
    username: string = '';
    dateOfBirth: string = '';

}
