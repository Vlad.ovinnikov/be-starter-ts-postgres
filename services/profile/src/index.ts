import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import { NextFunction, Request, Response } from 'express';
import * as helmet from 'helmet';
import { NOT_FOUND } from 'http-status-codes';
import 'reflect-metadata';

import { IConfig } from '../../../libs/microservices';

import { sequelize } from './db';
import routes from './routes';

const env: string = process.env.NODE_ENV || 'development';
const config: IConfig = require(`${__dirname}/../config/config.json`)[env];

const app: express.Application = express();
const PORT: number = config.apiPort;

// Application-Level Middleware
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(async (req: Request, res: Response, next: NextFunction) => {
  console.log(new Date());
  console.log(`Request ${req.method}: ${req.url}`);

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Expose-Headers', 'x-total-count');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  res.header('Access-Control-Allow-Headers', 'Content-Type,authorization');

  next();
});

// Routes
routes(app);

app.use((req: Request, res: Response) => {
  res.status(NOT_FOUND)
    .send('Not found');
});

sequelize.sync()
  .then(async () => {
    app.listen(PORT, () => {
      console.log(`*************** ${ config.name } running on: ***************`);
      console.log(`http://${ config.host }:${ PORT }`);
    });
});
