import * as axios from 'axios';
import { AxiosResponse, AxiosRequestConfig, Method } from 'axios';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import { Request, Response } from 'express';
import { NOT_FOUND, BAD_GATEWAY } from 'http-status-codes';
import parseEnvURL from './parseEnvURL';

const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.json`)[env];

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use(async (req: Request, res: Response): Promise<any> => {

  console.log(new Date());
  console.log(`${req.headers.host} - ${req.method}: ${req.url}`);
  const parts = req.url.split('/');
  const part = parts[1];
  const parsedEnvURL = parseEnvURL(part);

  if (!parsedEnvURL) {
    console.log('Can\'t parse url');

    return res.status(NOT_FOUND)
      .send(`Error parsing URL, service does not exist with path: ${ part }`);
  }

  const newUrl = `${parsedEnvURL()}${req.url}`;

  try {
    const requestConfig: AxiosRequestConfig = {
      headers: req.headers,
      url: newUrl,
      method: req.method as Method,
      data: req.body
    };

    const response: AxiosResponse = await axios.default(requestConfig);

    console.log(`Requested: ${parsedEnvURL()} - status: ${response.request.res.statusCode}`);

    return res
      .status(response.request.res.statusCode)
      .send(response.data);

  } catch (error) {
    if (!error.response) {
      console.log(`${req.method}: ${newUrl} - ${BAD_GATEWAY}: ${error.code}`);

      return res.status(BAD_GATEWAY)
        .send('The request can\'t proceed further, something wrong with connection ');
    }

    console.log(`${req.method}: ${newUrl} - ${error.response.status}: ${error.response.statusText}`);

    return res.status(error.response.status)
      .send(error.response.data);
  }
});

app.listen(config.apiPort, config.host, () => {
  console.log(`*************** ${ config.name } running on: ***************`);
  console.log(`http://${ config.host }:${ config.apiPort }`);
});
