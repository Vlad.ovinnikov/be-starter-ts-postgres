import { authAPI, profileAPI } from '../../../libs/env';

export default (url: string): any => {
    switch (url) {
        case 'auth':
            return authAPI;
        case 'profiles':
            return profileAPI;
        default:
            return '';
    }
};
