import { UserRole } from '../../../libs/commons';

export default async (Identity: any): Promise<any> => {
    await Identity.create(
        {
            username: 'SuperAdmin',
            email: 'admin@xp.com',
            emailConfirmed: true,
            role: UserRole.SuperAdmin,
            passwordHash: '$2a$10$hrFuKMbwWaICKnHIBw4Yhe99h7mOXUhCe4T5KMKnYsQz4lQSvga22', // Test1234!
            createdAt: new Date()
        }
    );
};
