'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('identities', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      email_confirmed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      password_hash: {
        type: Sequelize.STRING
      },
      role: {
        type: Sequelize.INTEGER
      },
      last_unsuccessful_login: {
        type: Sequelize.DATE,
        allowNull: true
      },
      last_successful_login: {
        type: Sequelize.DATE,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: true
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('identities');
  }
};
