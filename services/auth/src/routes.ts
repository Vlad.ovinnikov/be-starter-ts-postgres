import * as express from 'express';

import handlers from './handlers';

export default (app: express.Application): void => {
    app.use('/auth/identities', handlers.register);
};
