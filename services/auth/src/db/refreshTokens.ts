import { BuildOptions, DataTypes, Model } from 'sequelize';

import { sequelize, IdentityModel } from './index';

export interface IRefreshTokenModel extends Model {
  readonly refreshToken: string;
  readonly userId: number;
  readonly expiresAt: Date;
  readonly revoked: boolean;
  readonly createdAt: Date;
}

export type RefreshTokenStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): IRefreshTokenModel;
};

const RefreshTokenModel: RefreshTokenStatic = <RefreshTokenStatic>sequelize.define('refresh_tokens', {
  refreshToken: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.STRING
  },
  userId: {
    type: DataTypes.INTEGER
  },
  expiresAt: {
    allowNull: false,
    type: DataTypes.DATE
  },
  revoked: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: new Date()
  }
},
  {
    underscored: true,
    timestamps: false
  }
);

RefreshTokenModel.belongsTo(IdentityModel);
// RefreshTokenModel.associate = function(models) {
//     // associations can be defined here
//   };

module.exports = (): RefreshTokenStatic => RefreshTokenModel;
