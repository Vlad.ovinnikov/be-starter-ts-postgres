import { BuildOptions, DataTypes, Model } from 'sequelize';

import { sequelize } from './index';

export interface IIdentityModel extends Model {
  readonly userId: number;
  readonly username: string;
  readonly email: string;
  readonly emailConfirmed: boolean;
  readonly role: number;
  readonly passwordHash: string;
  readonly lastUnsuccessfulLogin?: Date;
  readonly lastSuccessfulLogin?: Date;
  readonly createdAt: Date;
  readonly updatedAt?: Date;
}

export type IdentityStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): IIdentityModel;

  findByLogin(login: any): Promise<IIdentityModel | null>;
};

const IdentityModel = <IdentityStatic>sequelize.define('identity', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true
  },
  username: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: false,
    validate: {
      len: {
        args: [1, 20],
        msg: 'Username should not be empty or more then 20 characters'
      }
    }
  },
  email: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: false,
    validate: {
      isEmail: true
    }
  },
  emailConfirmed: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  role: {
    type: DataTypes.INTEGER // SuperAdmin - 0, Admin - 1, User - 2
  },
  passwordHash: {
    type: DataTypes.STRING
  },
  lastUnsuccessfulLogin: {
    type: DataTypes.DATE,
    allowNull: true
  },
  lastSuccessfulLogin: {
    type: DataTypes.DATE,
    allowNull: true
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: new Date()
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: true
  }
}, {
  underscored: true,
  timestamps: false
});

IdentityModel.findByLogin = async (login: any): Promise<IIdentityModel | null> => {
  let user = await IdentityModel.findOne({
    where: { username: login }
  });

  if (!user) {
    user = await IdentityModel.findOne({
      where: { email: login }
    });
  }

  return user;
};

module.exports = (): IdentityStatic => IdentityModel;
