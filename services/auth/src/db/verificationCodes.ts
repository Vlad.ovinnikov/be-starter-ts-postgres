import { BuildOptions, DataTypes, Model } from 'sequelize';

import { sequelize } from './index';

export interface IVerificationCodeModel extends Model {
  readonly code: string;
  readonly userId: string;

  readonly email: string;
  readonly emailConfirmed: boolean;
  readonly role: number;
  readonly passwordHash: string;
  readonly lastUnsuccessfulLogin: Date;
  readonly lastSuccessfulLogin: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export type VerificationCodeStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): IVerificationCodeModel;

  associate(models: any): void;
};

const VerificationCodeModel = <VerificationCodeStatic>sequelize.define('verification_codes', {
  code: {
    type: DataTypes.TEXT,
    allowNull: false,
    primaryKey: true
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    onDelete: 'CASCADE',
    references: {
      model: 'identities',
      key: 'id'
      // deferrable: Deferrable.INITIALLY_IMMEDIATE
    }
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  used: DataTypes.BOOLEAN,
  createdAt: DataTypes.DATE
}, {
    underscored: true,
    timestamps: false
});

// VerificationCodeModel.hasOne(IdentityModel, {
//   foreignKey: { name: 'id', allowNull: false },
//   onDelete: 'CASCADE'
// });

module.exports = (): VerificationCodeStatic => VerificationCodeModel;
