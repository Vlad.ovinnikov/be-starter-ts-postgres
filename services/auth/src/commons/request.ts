import { Request } from 'express';

export interface ICreateRequest<T> extends Request {
    body: T;
}
