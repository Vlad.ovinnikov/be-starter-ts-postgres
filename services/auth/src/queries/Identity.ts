import { IIdentityModel, IdentityModel } from '../db';
import { Identity } from '../models';

export const register = async (idenity: Identity): Promise<IIdentityModel> => IdentityModel.create(idenity);
