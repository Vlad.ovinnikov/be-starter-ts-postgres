import { Request, Response, Router, NextFunction } from 'express';
import { CREATED, OK, INTERNAL_SERVER_ERROR, NOT_FOUND } from 'http-status-codes'; // CONFLICT
import { isString } from 'lodash';

import { postProfileService } from '../../../../libs/commons';
import { normalizeEmail } from '../../../../libs/microservices';

import { ICreateRequest } from '../commons';
import { IIdentityModel, IdentityModel } from '../db';
import { validate } from '../helpers';
import { IIdentityCreateData, IdentityCreateData, Identity } from '../models';
// import query from '../queries';

const router: Router = Router();

enum RegiterErrors {
  Internal = 'Internal'
}

router.post(
  '/register',
  validate(IdentityCreateData),
  async (req: ICreateRequest<IIdentityCreateData>, res: Response, next: NextFunction): Promise<Response> => {
    try {

      const createData: IIdentityCreateData = req.body;

      const email = normalizeEmail(createData.email);

      if (!isString(email)) {
        throw email;
      }

      const idenity = new Identity();
      idenity.email = email;
      idenity.username = createData.username;
      idenity.dateOfBirth = createData.dateOfBirth;
      idenity.passwordHash = createData.password;

      idenity.hashPassword();

      const profileResponse = await postProfileService('create-empty', {
        email,
        username: createData.username
      });

      console.log('========', profileResponse);

      return res.status(CREATED)
        .send(idenity);

      // const result = await query.register(idenity)
      //   .catch((e: any) => {
      //     if (e.name === ModelError.Constraint) {
      //       if (e.fields.email) {
      //         res.status(CONFLICT)
      //           .send('Email already in use');
      //       }

      //       if (e.fields.username) {
      //         res.status(CONFLICT)
      //           .send('Username already exists');
      //       }
      //     }
      //     throw new Error(RegiterErrors.Internal);
      //   });

      // return res.status(CREATED)
      //   .send(result);

    } catch (error) {
      if (error.message === RegiterErrors.Internal) {
        return res.status(INTERNAL_SERVER_ERROR)
          .send(`Error register user: ${ error.message }`);
      }

      return res.status(INTERNAL_SERVER_ERROR)
        .send(`Error register user: ${ error.message }`);
    }
});

router.get(
  '/',
  async (_req: Request, res: Response): Promise<Response> => {
  try {
    const identities: IIdentityModel[] = await IdentityModel.findAll();

    return res.status(OK)
      .send(identities);

  } catch (error) {
    return res.status(INTERNAL_SERVER_ERROR)
      .send(`Error getting identities: ${ error }`);
  }
});

router.get(
  '/:userId',
  async (req: Request, res: Response): Promise<Response> => {
  try {
    const user: IIdentityModel | null = await IdentityModel.findByPk(
      req.params.userId
    );

    if (!user) {
      return res.status(NOT_FOUND)
        .send('User not found');
    }

    return res.status(OK)
      .send(user);

  } catch (error) {
    return res.status(INTERNAL_SERVER_ERROR)
      .send(`Error getting identities: ${ error }`);
  }
});

export default router;
