import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'dateOfBirth', async: false })
export class BirthDateValidate implements ValidatorConstraintInterface {

  validate(text: string, validationArguments: ValidationArguments): boolean {
    return /^(\d{4})[\-](\d{2})[\-](\d{2})$/.test(text);
  }

  defaultMessage(args: ValidationArguments): string {
    return 'Date of birth is not valid, should be yyyy-mm-dd';
  }
}
