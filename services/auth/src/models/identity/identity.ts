import * as bcrypt from 'bcryptjs';
import { UserRole } from '../../../../../libs/commons';

export interface IIdentity  {
  readonly username: string;
  readonly email: string;
  readonly role: UserRole;
  readonly dateOfBirth: string;
  readonly passwordHash: string;
  // readonly createdAt: Date;
}

export class Identity implements IIdentity {

  username: string = '';
  email: string = '';
  role: UserRole = UserRole.User;
  dateOfBirth: string = '';
  passwordHash: string = '';
  // createdAt: Date = new Date();

  hashPassword(): void {
    this.passwordHash = bcrypt.hashSync(this.passwordHash, 10);
  }

  // checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
  //   return bcrypt.compareSync(unencryptedPassword, this.password);
  // }
}
