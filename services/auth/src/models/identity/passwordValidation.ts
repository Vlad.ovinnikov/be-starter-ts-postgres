import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ name: 'password', async: false })
export class PasswordValidate implements ValidatorConstraintInterface {

  validate(password: string, validationArguments: ValidationArguments): boolean {
    return password.length > 8 &&
      /\d/.test(password) &&
      (/[A-Z]/.test(password)) &&
      (/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/.test(password));
  }

  defaultMessage(args: ValidationArguments): string {
    return 'Password should have at least 8 characters and includes letters, numbers and symbols';
  }
}
