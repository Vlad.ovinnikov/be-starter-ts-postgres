import { Length, IsEmail, Validate } from 'class-validator';

import { BirthDateValidate } from './birthDateValidation';
import { PasswordValidate } from './passwordValidation';

export interface IIdentityCreateData  {
  readonly username: string;
  readonly email: string;
  readonly dateOfBirth: string;
  readonly password: string;
}

export class IdentityCreateData implements IIdentityCreateData {
  @Length(1, 250, {
    message: 'Username should not be empty or more then 20 characters'
  })
  username: string = '';

  @IsEmail()
  email: string = '';

  @Validate(BirthDateValidate)
  dateOfBirth: string = '';

  @Validate(PasswordValidate)
  password: string = '';
}
