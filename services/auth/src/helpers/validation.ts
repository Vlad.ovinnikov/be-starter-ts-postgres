import { Validator, ValidationError } from 'class-validator';
import { Request, Response, NextFunction, RequestHandler } from 'express';
import { INTERNAL_SERVER_ERROR, BAD_REQUEST } from 'http-status-codes';
import { deserialize } from 'json-typescript-mapper';

// Because all type information is erased in the compiled
// JavaScript, we can use this clever structural-typing
// work-around enabled by TypeScript to pass in a class
// to our middleware.
type Constructor<T> = { new(): T };

// This function returns a middleware which validates that the
// request's JSON body conforms to the passed-in type.
export const validate = <T>(type: Constructor<T>): RequestHandler => {
    const validator: Validator = new Validator();

    return async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            const input: any = deserialize(type, req.body);
            const errors: ValidationError[] = validator.validateSync(input, {
                validationError: {
                    target: false,
                    value: true
                }
            });

            if (errors.length > 0 && errors instanceof Array && errors[0] instanceof ValidationError) {
                return res.status(BAD_REQUEST)
                    .json({ errors })
                    .end();
            }

            req.body = input;

            return next();

        } catch (error) {
            return res.status(INTERNAL_SERVER_ERROR)
                .send(`Error: ${ error }`);
        }
    };
};
