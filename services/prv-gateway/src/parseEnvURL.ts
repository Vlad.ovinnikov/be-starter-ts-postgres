import { profileAPI } from '../../../libs/env';

export default (url: string): any => {
    switch (url) {
        case 'profiles':
            return profileAPI;
        default:
            return '';
    }
};
